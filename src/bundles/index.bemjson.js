module.exports = {
    block: 'page',
    title: 'Пустая',
    content: [
        {block: 'scheme', content: [
            {elem: 'title', content: 'Задачи проекта "Цифровой HR крупной компании'},
            {block: 'svg', attrs: {viewBox: '0 0 600 200'}, content: [
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 50, 70 L 50, 40 L 300, 40 L 300, 00', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 5, 80 L 10, 70 L 85, 70 L 90, 80', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 15, dy: '1.5em'}, content: 'Сохранение'},
                        {block: 'tspan', attrs: {x: 25, dy: '1.5em'}, content: 'чувства'},
                        {block: 'tspan', attrs: {x: 10, dy: '1.5em'}, content: '“командности”'},
                        {block: 'tspan', attrs: {x: 18, dy: '1.5em'}, content: 'в большом'},
                        {block: 'tspan', attrs: {x: 16, dy: '1.5em'}, content: 'коллективе'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 180, 70 L 180, 40 L 300, 40 L 300, 00', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 125, 80 L 130, 70 L 220, 70 L 225, 80', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 155, dy: '1.5em'}, content: 'Быстрая'},
                        {block: 'tspan', attrs: {x: 125, dy: '1.5em'}, content: 'адаптация нового'},
                        {block: 'tspan', attrs: {x: 140, dy: '1.5em'}, content: 'сотрудника к'},
                        {block: 'tspan', attrs: {x: 150, dy: '1.5em'}, content: 'работе в'},
                        {block: 'tspan', attrs: {x: 147, dy: '1.5em'}, content: 'компании'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 300, 70 L 300, 40 L 300, 40 L 300, 00', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 245, 80 L 250, 70 L 350, 70 L 355, 80', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 275, dy: '1.5em'}, content: 'Контроль'},
                        {block: 'tspan', attrs: {x: 250, dy: '1.5em'}, content: 'процессов найма'},
                        {block: 'tspan', attrs: {x: 270, dy: '1.5em'}, content: 'для бизнес-'},
                        {block: 'tspan', attrs: {x: 260, dy: '1.5em'}, content: 'заказчика, HRD'},
                        {block: 'tspan', attrs: {x: 263, dy: '1.5em'}, content: 'собственника'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 430, 70 L 430, 40 L 300, 40 L 300, 00', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 380, 80 L 385, 70 L 475, 70 L 480, 80', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 400, dy: '1.5em'}, content: 'Улучшение'},
                        {block: 'tspan', attrs: {x: 390, dy: '1.5em'}, content: 'коммуникации,'},
                        {block: 'tspan', attrs: {x: 385, dy: '1.5em'}, content: 'создания единого'},
                        {block: 'tspan', attrs: {x: 383, dy: '1.5em'}, content: 'информационного'},
                        {block: 'tspan', attrs: {x: 420, dy: '1.5em'}, content: 'поля'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 550, 70 L 550, 40 L 300, 40 L 300, 00', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 505, 80 L 510, 70 L 590, 70 L 595, 80', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 520, dy: '1.5em'}, content: 'Регулярное'},
                        {block: 'tspan', attrs: {x: 525, dy: '1.5em'}, content: 'обучение'},
                        {block: 'tspan', attrs: {x: 510, dy: '1.5em'}, content: 'сотрудников и'},
                        {block: 'tspan', attrs: {x: 510, dy: '1.5em'}, content: 'повышение их'},
                        {block: 'tspan', attrs: {x: 510, dy: '1.5em'}, content: 'квалификации'},
                    ]},
                ]},
            ]},
        ]},
        {block: 'scheme', content: [
            {elem: 'title', content: 'Причины внедрения HR-портала фармацевтической компании'},
            {block: 'svg', attrs: {viewBox: '0 0 600 200'}, content: [
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 180, 70 L 180, 40 L 300, 40 L 300, 00', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 125, 80 L 130, 70 L 220, 70 L 225, 80', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 150, dy: '1.5em'}, content: 'Сложность'},
                        {block: 'tspan', attrs: {x: 150, dy: '1.5em'}, content: 'процессов'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 180, 120 L 180, 170', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 115, 180 L 120, 170 L 230, 170 L 235, 180', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', mods: {small: true}, attrs: {y: 170, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 120, dy: '1.5em'}, content: 'до 100 часов на нового'},
                        {block: 'tspan', attrs: {x: 117, dy: '1.5em'}, content: 'сотрудника, до 3 часов в'},
                        {block: 'tspan', attrs: {x: 130, dy: '1.5em'}, content: 'месяц на развитие'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 300, 70 L 300, 40 L 300, 40 L 300, 00', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 245, 80 L 250, 70 L 350, 70 L 355, 80', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 265, dy: '1.5em'}, content: 'Численность'},
                        {block: 'tspan', attrs: {x: 275, dy: '1.5em'}, content: 'команды'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 300, 120 L 300, 170', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 245, 180 L 250, 170 L 350, 170 L 355, 180', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', mods: {small: true}, attrs: {y: 170, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 255, dy: '1.5em'}, content: '2000 сотрудников ='},
                        {block: 'tspan', attrs: {x: 270, dy: '1.5em'}, content: 'минимум 30'},
                        {block: 'tspan', attrs: {x: 255, dy: '1.5em'}, content: 'новичков в месяц'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 430, 70 L 430, 40 L 300, 40 L 300, 00', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 380, 80 L 385, 70 L 475, 70 L 480, 80', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 410, dy: '1.5em'}, content: 'Потери'},
                        {block: 'tspan', attrs: {x: 390, dy: '1.5em'}, content: 'эффективности'},
                    ]},
                ]},
                {block: 'g', content: [
                    {block: 'path', attrs: {d: 'M 430, 120 L 430, 170', stroke: 'grey', fill: 'transparent'}},
                    {block: 'path', attrs: {d: 'M 370, 180 L 375, 170 L 485, 170 L 490, 180', stroke: 'grey', fill: 'transparent'}},
                    {block: 'text', mods: {small: true}, attrs: {y: 170, fill: 'grey'}, content: [
                        {block: 'tspan', attrs: {x: 385, dy: '1.5em'}, content: 'В каждом процессе'},
                        {block: 'tspan', attrs: {x: 385, dy: '1.5em'}, content: 'участвует минимум'},
                        {block: 'tspan', attrs: {x: 382, dy: '1.5em'}, content: '5 человек. Требуется'},
                        {block: 'tspan', attrs: {x: 387, dy: '1.5em'}, content: 'неделя на каждое'},
                        {block: 'tspan', attrs: {x: 388, dy: '1.5em'}, content: 'простое действие'},
                    ]},
                ]},
            ]},
            {block: 'scheme', content: [
                {elem: 'title', content: 'Ценность HR-портала для крупной компании'},
                {block: 'svg', attrs: {viewBox: '0 0 665 200'}, content: [
                    {block: 'g', content: [
                        {block: 'path', attrs: {d: 'M 50, 70 L 50, 40 L 335, 40 L 335, 00', stroke: 'grey', fill: 'transparent'}},
                        {block: 'path', attrs: {d: 'M 5, 80 L 10, 70 L 85, 70 L 90, 80', stroke: 'grey', fill: 'transparent'}},
                        {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                            {block: 'tspan', attrs: {x: 25, dy: '1.5em'}, content: 'Функции:'},
                            {block: 'tspan', attrs: {x: 3, dy: '1.5em'}, content: 'Рекрутинг, найм,'},
                            {block: 'tspan', attrs: {x: 15, dy: '1.5em'}, content: 'адаптация и'},
                            {block: 'tspan', attrs: {x: 25, dy: '1.5em'}, content: 'развитие'},
                            {block: 'tspan', attrs: {x: 15, dy: '1.5em'}, content: 'сотрудника в'},
                            {block: 'tspan', attrs: {x: 3, dy: '1.5em'}, content: 'едином решении'},
                        ]},
                    ]},
                    {block: 'g', content: [
                        {block: 'path', attrs: {d: 'M 160, 70 L 160, 40 L 335, 40 L 335, 00', stroke: 'grey', fill: 'transparent'}},
                        {block: 'path', attrs: {d: 'M 100, 80 L 105, 70 L 205, 70 L 210, 80', stroke: 'grey', fill: 'transparent'}},
                        {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                            {block: 'tspan', attrs: {x: 105, dy: '1.5em'}, content: 'Для собственника:'},
                            {block: 'tspan', attrs: {x: 115, dy: '1.5em'}, content: 'прозрачность'},
                            {block: 'tspan', attrs: {x: 110, dy: '1.5em'}, content: 'процесса найма'},
                        ]},
                    ]},
                    {block: 'g', content: [
                        {block: 'path', attrs: {d: 'M 275, 70 L 275, 40 L 335, 40 L 335, 00', stroke: 'grey', fill: 'transparent'}},
                        {block: 'path', attrs: {d: 'M 220, 80 L 225, 70 L 325, 70 L 330, 80', stroke: 'grey', fill: 'transparent'}},
                        {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                            {block: 'tspan', attrs: {x: 240, dy: '1.5em'}, content: 'Для бизнеса'},
                            {block: 'tspan', attrs: {x: 250, dy: '1.5em'}, content: 'в целом:'},
                            {block: 'tspan', attrs: {x: 230, dy: '1.5em'}, content: 'ускорение найма'},
                        ]},
                    ]},
                    {block: 'g', content: [
                        {block: 'path', attrs: {d: 'M 390, 70 L 390, 40 L 335, 40 L 335, 00', stroke: 'grey', fill: 'transparent'}},
                        {block: 'path', attrs: {d: 'M 340, 80 L 345, 70 L 435, 70 L 440, 80', stroke: 'grey', fill: 'transparent'}},
                        {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                            {block: 'tspan', attrs: {x: 365, dy: '1.5em'}, content: 'Для HRD:'},
                            {block: 'tspan', attrs: {x: 340, dy: '1.5em'}, content: 'удобный контроль'},
                            {block: 'tspan', attrs: {x: 340, dy: '1.5em'}, content: 'найма, прозрачная'},
                            {block: 'tspan', attrs: {x: 360, dy: '1.5em'}, content: 'аналитика'},
                        ]},
                    ]},
                    {block: 'g', content: [
                        {block: 'path', attrs: {d: 'M 500, 70 L 500, 40 L 335, 40 L 335, 00', stroke: 'grey', fill: 'transparent'}},
                        {block: 'path', attrs: {d: 'M 450, 80 L 455, 70 L 545, 70 L 550, 80', stroke: 'grey', fill: 'transparent'}},
                        {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                            {block: 'tspan', attrs: {x: 460, dy: '1.5em'}, content: 'Для рекрутера:'},
                            {block: 'tspan', attrs: {x: 460, dy: '1.5em'}, content: 'автоматизация'},
                            {block: 'tspan', attrs: {x: 470, dy: '1.5em'}, content: 'рутины. Все'},
                            {block: 'tspan', attrs: {x: 460, dy: '1.5em'}, content: 'преимущества'},
                            {block: 'tspan', attrs: {x: 490, dy: '1.5em'}, content: 'CRM'},
                        ]},
                    ]},
                    {block: 'g', content: [
                        {block: 'path', attrs: {d: 'M 610, 70 L 610, 40 L 335, 40 L 335, 00', stroke: 'grey', fill: 'transparent'}},
                        {block: 'path', attrs: {d: 'M 560, 80 L 565, 70 L 655, 70 L 660, 80', stroke: 'grey', fill: 'transparent'}},
                        {block: 'text', attrs: {y: 70, fill: 'grey'}, content: [
                            {block: 'tspan', attrs: {x: 590, dy: '1.5em'}, content: 'Для ИТ:'},
                            {block: 'tspan', attrs: {x: 580, dy: '1.5em'}, content: 'нет новых'},
                            {block: 'tspan', attrs: {x: 580, dy: '1.5em'}, content: 'программ,'},
                            {block: 'tspan', attrs: {x: 580, dy: '1.5em'}, content: 'все внутри'},
                            {block: 'tspan', attrs: {x: 600, dy: '1.5em'}, content: 'Б24.'},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ],
};
